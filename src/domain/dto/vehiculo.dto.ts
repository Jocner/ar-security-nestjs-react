import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class vehiculoDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  patente: string;

  @ApiProperty()
  @IsString()
  marca: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  propietario: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  telefono: string;

  @ApiProperty()
  @IsString()
  image: string;
}
