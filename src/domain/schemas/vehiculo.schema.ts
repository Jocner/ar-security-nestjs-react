import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type VehiculoDocument = Vehiculo & Document;

@Schema()
export class Vehiculo {
  @Prop({ required: true })
  patente: string;

  @Prop({ required: false})
  marca: string;

  @Prop({ required: true })
  propietario: string;

  @Prop({ required: true })
  telefono: string;

  @Prop({ required: false })
  image: string;
  static patente: any;
}

export const VehiculoSchema = SchemaFactory.createForClass(Vehiculo);
