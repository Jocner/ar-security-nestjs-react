import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { vehiculoDTO } from '../../domain/dto/vehiculo.dto';
import {
  Vehiculo,
  VehiculoDocument,
} from '../../domain/schemas/vehiculo.schema';
import { Model } from 'mongoose';

@Injectable()
export class VehiculoCase {
  constructor(
    @InjectModel(Vehiculo.patente)
    private vehiculoModel: Model<VehiculoDocument>,
  ) {}

  async create(vehiculo: vehiculoDTO): Promise<any> {
    const newVehiculo = await this.vehiculoModel.create(vehiculo);

    return newVehiculo;
  }

  async findByPatente(vehiculo: vehiculoDTO): Promise<any> {
    const newVehiculo = await this.vehiculoModel.find({
      patente: vehiculo.patente,
    });

    return newVehiculo;
  }
}
