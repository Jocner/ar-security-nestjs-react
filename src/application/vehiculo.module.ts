import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { VehiculoSchema, Vehiculo } from 'src/domain/schemas/vehiculo.schema';
import { VehiculoCase } from './use-case/vehiculoCase';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Vehiculo.patente,
        schema: VehiculoSchema,
      },
    ]),
  ],
  providers: [VehiculoCase],
})
export class VehiculoModule {}
